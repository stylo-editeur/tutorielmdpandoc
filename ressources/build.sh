#!/bin/bash
# execute la commande pandoc plusieurs fois pour sortir un pdf et un epub à partir d'un fichier source monfichier.md et des métadonnées monfichier.yaml
# Pour lance ce script : bash build.sh

## Création du pdf
pandoc --standalone -f markdown -t latex monfichier.md monfichier.yaml -o monfichier.tex
xelatex monfichier.tex
xelatex monfichier.tex

## Création de l'epub
pandoc --standalone -f markdown -t epub monfichier.md monfichier.yaml -o monfichier.epub


# vous pouvez complexifiez ce script, notamment en passant au script un argument, par exemple, le fichier à convertir.
# cela permettra ensuite de lancer le même script ainsi : bash build.sh votrefichier.md

