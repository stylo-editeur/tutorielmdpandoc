# Tutoriel pour apprendre à utiliser la chaîne d'écriture markdown+pandoc

En premier lieu, vous pouvez [vous créer un compte sur Framagit](https://framagit.org/users/sign_in?redirect_to_referer=yes) et [installer les outils nécessaires](./parcours/03_outils.md).

## Introduction et théorie

0. Introduction générale ([parcours/00_introduction](./parcours/00_introduction.md))
1. Un peu de théorie ([parcours/01_theorie](./parcours/01_theorie.md))
    - Gestion de fichiers : arborescence, nomenclature et versionnage
    - Formats de texte
    - Chaines éditoriales
    - Edition savante

## L'outil Stylo

2. Prise en main de Stylo ([parcours/02_stylo](./parcours/02_stylo.md))
    - présentation
    - édition d'un article
    - export d'un article

3. Rédiger et éditer son mémoire avec Stylo ([parcours/02_stylo_memoire](./parcours/02_stylo_memoire.md))

## Edition avec Pandoc

4. Outils ([parcours/03_outils](./parcours/03_outils.md))
    - Installation des outils
    - Prise en main de git ([parcours/03b_git](./parcours/03b_git.md))

5. Exercice 2 - édition ([parcours/04_edition](./parcours/04_edition.md))
    - Prise en main de Pandoc
    - Exercice de conversion
    - Templating


## Editer un livre

Produire un ouvrage en HTML, Pdf (LaTeX) et Epub, à partir d'une édition Markdown.

6. Initialisation de l'ouvrage ([parcours/05_livre](./parcours/05_livre.md))
7. Production du HTML ([parcours/06_livreHTML](./parcours/06_livreHTML.md))
8. Production du PDF ([parcours/07_livreLatex](./parcours/07_livreLatex.md))
9. Production de l'Epub ([parcours/08_livreEpub](./parcours/08_livreEpub.md))


## Pour aller plus loin

1. **Créer une chaine** en automatisant une série d'actions ou de commandes en utilisant les scripts bash.
  - Par exemple, pour produire d'un coup un epub et un pdf à partir d'une source markdown, voir le script [ressources/build.sh](./ressources/build.sh).
  - Voir aussi [cette petite documentation](https://doc.ubuntu-fr.org/tutoriel/script_shell) pour créer des scripts.
2. **Utiliser des filtres** pour créer des traitements particuliers selon vos sorties (par exemple «supprimer tous les titres de niveau 3 pour les exports Pdf»).
  - Voir la documentation [Filtres](http://pandoc.org/filters.html) et [Filtres Lua](http://pandoc.org/lua-filters.html).
3. **Utiliser des [préprocesseurs](https://github.com/jgm/pandoc/wiki/Pandoc-Extras#preprocessors)** pour complexifier le markdown source.
4. **Consulter le [wiki Pandoc](https://github.com/jgm/pandoc/wiki)** qui référence de multiples ressources.

---
[Démarrer](./parcours/00_introduction.md)
