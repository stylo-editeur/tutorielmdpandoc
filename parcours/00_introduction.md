# Introduction générale

**Objectifs de ce tutoriel:**

- créer et éditer des documents divers
- devenir autonome dans l'utilisation de chaines de publications alternatives
- comprendre les enjeux liés aux formats et aux outils
- savoir trouver l'information qu'il vous manque



---
Voir la suite [[01_theorie](./01_theorie.md)]
