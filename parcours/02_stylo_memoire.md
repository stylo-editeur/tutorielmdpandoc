# Rédiger et éditer un mémoire avec Stylo

Les formats utilisés par Stylo sont des standards, il est possible de mettre en place des chaines de traitements pour sortir des objets éditoriaux autres que des articles.
En particulier, Stylo permet de produire des mémoires et des thèses.

<div style="border: 1px solid grey; background-color: whitesmoke; padding:10px;">

Attention: Stylo est actuellement en refonte. Nous vous présentons ici [la nouvelle version](https://alpha.stylo.14159.ninja/), en ligne **à titre de démonstration**. La nouvelle version sera bientôt disponible sur l'outil officiel.

</div>

## Principes de base

> Un mémoire est constitué de un ou plusieurs documents Stylo mis bout-à-bout.

- Ces documents peuvent être les chapitres ou les parties du mémoire.
- Ils sont rassemblés ensemble dans un mémoire à l'aide d'un même label _[tag]_, qui doit être associé à chaque document Stylo.
- Chaque chapitre fonctionne donc comme un document Stylo :
  - il possède ses propres métadonnées et sa propre bibliographie.
  - il peut être partagé en tant que tel (annotation, preview, etc.). C'est au moment de l'export du mémoire que les différentes parties sont éditées ensemble.
- Une interface dédiée _[My Books]_ permet d'ordonner les différents documents, et d'éditer les métadonnées du mémoire.
  <div style="border: 1px solid grey; background-color: whitesmoke; padding:10px;">

    Dans cette version de démo, les documents sont ordonnés par ordre alphabétique de titre de document Stylo (à ne pas confondre avec le `title` des métadonnées)

  </div>

## Quelques particularités

### Niveaux de titre

Votre mémoire ou thèse peut être structuré en parties et chapitres, ou en chapitres seuls.

Les titres de **partie** doivent être des **titres de niveau 1** (exemple: `# Première Partie : mon titre de partie`) et les titres de **chapitre** seront alors des **titres de niveau 2**.

Dans le cas d'un mémoire structuré en chapitres seuls, les titres de **chapitre** seront des **titres de niveau 1** (exemple: `# mon titre de chapitre`)

Au moment de l'export, vous pourrez déclarer l'organisation de votre mémoire :

1. **en parties et chapitres**
2. **en chapitres seuls**


### Bibliographie

Dans un mémoire ou une thèse, la bibliographie est souvent divisées en différentes sections. Stylo permet de créer une bibliographie organisée en sous-sections.

En deux étapes :

1. dans les métadonnées du mémoire, il faut déclarer les différentes sections de la bibliographie. Pour cela, passez les métadonnées en mode raw ![rawmode](../media/alpha_rawmode.png), puis ajoutez avant `---` les lignes suivantes  :

    ```yaml
    subbiblio:
      - key: pratique
        title: Pratique littéraire
      - key: theorie
        title: Théorie
    ```

    La structure est la suivante :
    - `key` est la "clé de section", autrement dit un tag qui sera utilisé à l'étape suivante
    - `title` sera votre titre de section de bibliographie, tel qu'il sera affiché dans le mémoire.

2. pour chacune des références bibliographiques concernée, ajoutez dans le champs `keywords` la clé de section (ex: `pratique`, ou `theorie`). Cette étape peut être faite soit dans Zotero, soit dans Stylo, en éditant le bibtex directement.

### Métadonnées du mémoire

Dans une prochaine version, l'interface "My Books" proposera un éditeur de métadonnées pour les métadonnées du mémoire ou de la thèse.

<div style="border: 1px solid grey; background-color: whitesmoke; padding:10px;">

Dans cette version de démo, les métadonnées du mémoire seront celles du premier document déclaré. Les autres métadonnées sont ignorées.

Les sous-divisions de la bibliographie doivent donc être déclarées dans le premier document du mémoire.

</div>


### Export

L'export du mémoire se fait à travers un template LaTeX dédié. Il correspond au template de mémoire et de thèse de l'Université de Montréal.

D'autres templates (modèles) seront disponibles prochainement.

![exportbook](../media/alpha_exportbook.png)

Plusieurs options sont disponibles :

1. format du document exporté
2. style bibliographique
3. table des matières
4. numérotation (ou non) des sections et chapitres
5. structure du mémoire : en parties et chapitres, ou en chapitres seuls.


### Personnaliser l'export en pdf

- insertion de code Latex dans le md.
